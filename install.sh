# copy to .local/bin/
cp -vf $PWD/make-gitignore.sh $HOME/.local/bin/make-gitignore

# replace setup.conf by $PWD/setup.conf
sed -i "s|setup.conf|$PWD/setup.conf|g" $HOME/.local/bin/make-gitignore

# print installed
echo "Installation done."
