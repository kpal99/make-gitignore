## Make gitignore

Make gitignore for a project by downloading gitignore templates
from github or gitlab.

Enter `SOURCE` and `USERNAME` in [setup.conf](./setup.conf).

### Install
- Clone this repo
```sh
source install.sh
```

### Usage
```sh
> ./make-gitignore.sh -h
Usage: ./make-gitignore.sh -t <type1,type2,..>
```

P.S: if `SOURCE` and `USERNAME` is `github`, use C%2B%2B instead of C++ and be mindful of symlinks of Clojure, Fortran, Kotlin as these wouldn't be behave right

### TODO:
- [ ] Add flag -P for wget to download from within the folder of [gitignore repo](https://gitlab.com/kpal99/gitignore)
