#!/bin/bash
source setup.conf

# read arguments
while getopts "ht:" opt;
do
    case $opt in
        t)  types=$OPTARG ;;
        h)  echo "Usage: $0 -t <type1,type2,..>" >&2
            exit 0 ;;
        \?) exit 1 ;;
    esac
done

# Read comma-separated types in array
IFS=',' read -r -a typesArray <<< "$types"

# if $source=gitlab do this
if [ $SOURCE == "gitlab" ]; then
    URL="https://gitlab.com/$USERNAME/gitignore/-/raw/main/"
elif [ $SOURCE == "github" ]; then
    URL="https://raw.githubusercontent.com/$USERNAME/gitignore/main/"
fi

[[ ! -e .gitignore ]] && wget --no-verbose "$URL"Gitignore -O .gitignore || echo ".gitignore exists"
for type in "${typesArray[@]}";
do
    wget --no-verbose "$URL$type.gitignore" -O /tmp/$type.gitignore
    echo >> .gitignore
    echo "########################################################" >> .gitignore
    echo "# $URL$type.gitignore" >> .gitignore
    echo "########################################################" >> .gitignore
    echo "cat /tmp/$type.gitignore >> .gitignore"
    cat /tmp/$type.gitignore >> .gitignore
    echo
done
